# QRエンコードライブラリ／サンプル

本資料はQRコードのエンコード用ライブラリと、そのサンプルです。

# Javaライブラリ
srcフォルダ以下にあります。

QrCoderEncoder.java

がメインのライブラリとなります。

# Java実行サンプル
testフォルダ以下にあります。


# Java依存ライブラリ
libフォルダ以下にあります。
Apache CommonsおよびantなどはADWORLDで利用されているものを流用しています。
新規に追加したのはgroovy-all-1.8.8.jarのみです。


# JavaScript実行サンプル
webフォルダ以下にあるtest.htmlを参照してください。
web上で、読込件数突合せとヘッダ文字列比較を実行しています。


# JSON生成ツール
toolsフォルダにあります。
Csv2Json.batを実行すると、csvフォルダ内のheader/qrbody.csvを参照して、
jsonフォルダにjsonファイルを出力します。
