package com.hitachijoho.nextadworld.dlayer.pgo;

import java.io.Serializable;

public class Jkr001_Bean implements Serializable {
	public Jkr001_Bean() {
	}

	private String aid;
	private String sid;
	private String adate;
	private String agent;
	private String copies;
	private String memo;
	private String ptn;
	private String style;
	private String certificate;

	private Applicant applicant;
	private Matters matters;
	private Seeking seeking;

	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getAdate() {
		return adate;
	}

	public void setAdate(String adate) {
		this.adate = adate;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getCopies() {
		return copies;
	}

	public void setCopies(String copies) {
		this.copies = copies;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public String getPtn() {
		return ptn;
	}

	public void setPtn(String ptn) {
		this.ptn = ptn;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public Matters getMatters() {
		return matters;
	}

	public void setMatters(Matters matters) {
		this.matters = matters;
	}

	public Seeking getSeeking() {
		return seeking;
	}

	public void setSeeking(Seeking seeking) {
		this.seeking = seeking;
	}

	public class Applicant implements Serializable {
		public Applicant() {

		}

		private String fname;
		private String kname;
		private String birth;
		private String sex;
		private String sexdflag;

		public String getFname() {
			return fname;
		}

		public void setFname(String fname) {
			this.fname = fname;
		}

		public String getKname() {
			return kname;
		}

		public void setKname(String kname) {
			this.kname = kname;
		}

		public String getBirth() {
			return birth;
		}

		public void setBirth(String birth) {
			this.birth = birth;
		}

		public String getSex() {
			return sex;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getSexdflag() {
			return sexdflag;
		}

		public void setSexdflag(String sexdflag) {
			this.sexdflag = sexdflag;
		}

		public String getAdr() {
			return adr;
		}

		public void setAdr(String adr) {
			this.adr = adr;
		}

		public String getDivision() {
			return division;
		}

		public void setDivision(String division) {
			this.division = division;
		}

		private String adr;
		private String division;
	}

	public class Seeking implements Serializable {
		public Seeking() {

		}

		private String fname;
		private String kname;
		private String birth;
		private String sex;
		private String sexdflag;
		private String division;

		public String getFname() {
			return fname;
		}

		public void setFname(String fname) {
			this.fname = fname;
		}

		public String getKname() {
			return kname;
		}

		public void setKname(String kname) {
			this.kname = kname;
		}

		public String getBirth() {
			return birth;
		}

		public void setBirth(String birth) {
			this.birth = birth;
		}

		public String getSex() {
			return sex;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getSexdflag() {
			return sexdflag;
		}

		public void setSexdflag(String sexdflag) {
			this.sexdflag = sexdflag;
		}

		public String getDivision() {
			return division;
		}

		public void setDivision(String division) {
			this.division = division;
		}

		public String getAdr() {
			return adr;
		}

		public void setAdr(String adr) {
			this.adr = adr;
		}

		private String adr;
	}

	public class Matters implements Serializable {
		public Matters() {

		}

		private String principal;
		private String fname;
		private String ksname;
		private String head;
		private String badr;
		private String calligraphy;
		private String relation;
		private String oadr;
		private String remarks;
		private String mynum;
		private String residentcode;
		private String padr;
		private String fremarks;
		private String a30c45;
		private String residence;
		private String residencecard;
		private String nationality;
		private String period;
		private String edate;

		public String getPrincipal() {
			return principal;
		}

		public void setPrincipal(String principal) {
			this.principal = principal;
		}

		public String getFname() {
			return fname;
		}

		public void setFname(String fname) {
			this.fname = fname;
		}

		public String getKsname() {
			return ksname;
		}

		public void setKsname(String ksname) {
			this.ksname = ksname;
		}

		public String getHead() {
			return head;
		}

		public void setHead(String head) {
			this.head = head;
		}

		public String getBadr() {
			return badr;
		}

		public void setBadr(String badr) {
			this.badr = badr;
		}

		public String getCalligraphy() {
			return calligraphy;
		}

		public void setCalligraphy(String calligraphy) {
			this.calligraphy = calligraphy;
		}

		public String getRelation() {
			return relation;
		}

		public void setRelation(String relation) {
			this.relation = relation;
		}

		public String getOadr() {
			return oadr;
		}

		public void setOadr(String oadr) {
			this.oadr = oadr;
		}

		public String getRemarks() {
			return remarks;
		}

		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		public String getMynum() {
			return mynum;
		}

		public void setMynum(String mynum) {
			this.mynum = mynum;
		}

		public String getResidentcode() {
			return residentcode;
		}

		public void setResidentcode(String residentcode) {
			this.residentcode = residentcode;
		}

		public String getPadr() {
			return padr;
		}

		public void setPadr(String padr) {
			this.padr = padr;
		}

		public String getFremarks() {
			return fremarks;
		}

		public void setFremarks(String fremarks) {
			this.fremarks = fremarks;
		}

		public String getA30c45() {
			return a30c45;
		}

		public void setA30c45(String a30c45) {
			this.a30c45 = a30c45;
		}

		public String getResidence() {
			return residence;
		}

		public void setResidence(String residence) {
			this.residence = residence;
		}

		public String getResidencecard() {
			return residencecard;
		}

		public void setResidencecard(String residencecard) {
			this.residencecard = residencecard;
		}

		public String getNationality() {
			return nationality;
		}

		public void setNationality(String nationality) {
			this.nationality = nationality;
		}

		public String getPeriod() {
			return period;
		}

		public void setPeriod(String period) {
			this.period = period;
		}

		public String getEdate() {
			return edate;
		}

		public void setEdate(String edate) {
			this.edate = edate;
		}

	}

}
