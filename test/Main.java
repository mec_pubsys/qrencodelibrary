import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import com.hitachijoho.nextadworld.common.qrcode.QrCoderEncoder;

public class Main {

	public static void main(String[] args) throws Throwable {

		// QRR[hðÇÝo·³Ìt@C
		String[][] filePathlist = new String[][] {
				{ "data/Z¯[QR1.txt", "data/Z¯[QR2.txt", "data/Z¯[QR3.txt", "data/Z¯[QR4.txt", "data/Z¯[QR5.txt",
						"data/Z¯[QR6.txt" },
				{ "data/óÓo^QR1.txt", "data/óÓo^QR2.txt", "data/óÓo^QR3.txt", "data/óÓo^QR4.txt", "data/óÓo^QR5.txt" },
				{ "data/©ÈQR.txt" } };
		for (String[] filePaths : filePathlist) {

			// generate Encoder Instance.
			QrCoderEncoder encoder = new QrCoderEncoder();

			for (String filePath : filePaths) {
				// read json text from file and encode.
				String txt = readFile(filePath);
				boolean result = encoder.readQrCode(txt);
				writeLog("[" + filePath + "] -> " + encoder.getImported() + "/" + encoder.getTotal());

				if (result) {
					// if all json imported,end loop.
					writeLog("### All QR-Code read done. ###");
					break;
				} else {
					writeLog("### need to read more QR-Code. ###");
				}
			}
			Map<String, Object> map = encoder.getDataMap();
			for (String key : map.keySet()) {
				writeLog(key + ":" + map.get(key));
			}
		}
	}

	private static String readFile(String filePath) {
		File file = new File(filePath);
		StringBuilder buf = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String text;
			while ((text = br.readLine()) != null) {
				buf.append(text);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
		return buf.toString();
	}

	private static void writeLog(String text) {
		System.out.println(text);
		File file = new File("out.log");
		StringBuilder buf = new StringBuilder();
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file, true));
			bw.write(text);
			bw.newLine();
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}
}
