import groovy.json.*;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;
import java.nio.charset.Charset;
import javax.xml.bind.*;



def encrypt(String str) {
	String CRYPT_KEY="encodekey1234567";
	byte[] bytes = str.getBytes();
	SecretKeySpec secretkey = new SecretKeySpec(CRYPT_KEY.getBytes(Charset.forName("UTF-8")), "AES");
	def cph = Cipher.getInstance("AES");
	cph.init(Cipher.ENCRYPT_MODE, secretkey);
	def enc = cph.doFinal(bytes);
	String hexString = DatatypeConverter.printHexBinary(enc);
	return hexString;
}

def decrypt(String str) {
	String CRYPT_KEY="encodekey1234567";
	def bytes = DatatypeConverter.parseHexBinary(str); 
	SecretKeySpec secretkey = new SecretKeySpec(CRYPT_KEY.getBytes(Charset.forName("UTF-8")), "AES");
	def cph = Cipher.getInstance("AES");
	cph.init(Cipher.DECRYPT_MODE, secretkey);
	return new String(cph.doFinal(bytes));
}

// QRコードのデータサイズ
int sz=500

// CSVからテスト用JSONを作成する。
// レベル１～３まで固定。

// データ部分作成
Map qrbody = [:];
def lastkey1="";
def lastkey2="";
def lastkey3="";
new File("csv/qrbody.csv").splitEachLine(","){line ->
	def lv = line[0];
	def key=line[1];
	def val=line[2];
	if(lv=="1" && val != null){
		qrbody.put(key,val);
	}else if(lv=="1" && val == null){
		lastkey1 = key;
		qrbody.put(key,[:]);
	}else if(lv=="2"){
		def map1 = qrbody[lastkey1];
		map1[key] = val;
	}else if(lv=="3"){
		def map1 = qrbody[lastkey1];
		map1[key] = val;
	}
}
// MapからJSON化→BASE64エンコーディングして、適切なサイズに分割
def json_body = JsonOutput.toJson(qrbody);
new File("json/body.json").text = JsonOutput.prettyPrint(json_body);
def bs64body = json_body.getBytes().encodeBase64().toString();
def encbody = encrypt(bs64body);
// if not use base64
encbody = encrypt(json_body);

// ヘッダ読込→Map作成
Map keys = [:];
new File("csv/header.csv").splitEachLine(","){line ->
	def lv = line[0];
	def key=line[1];
	def val=line[2];
	keys.put(key,val);
}
def json_keys = JsonOutput.toJson(keys);
new File("json/header.json").text = JsonOutput.prettyPrint(json_keys);
def bs64keys = json_keys.getBytes().encodeBase64().toString();
def enckeys = encrypt(bs64keys);
// if not use base64
enckeys = encrypt(json_keys);

int tnum = (int) encbody.length()/sz + 1;
qrdata = [:];

for(int i=0;i<encbody.length()/sz;i++){
	int start = i*sz;
	int end = (start+sz > encbody.length())?encbody.length():start + sz;
	body = encbody.substring(start,end);
	qrdata.put("tnum",tnum);
	qrdata.put("snum",(i+1));
	qrdata.put("keys",enckeys);
	qrdata.put("qrbody",body);

	def json = JsonOutput.prettyPrint(JsonOutput.toJson(qrdata));

	new File("json/sample_${i+1}.json").withWriter("UTF-8"){out ->
		out.println json;
	}
}

