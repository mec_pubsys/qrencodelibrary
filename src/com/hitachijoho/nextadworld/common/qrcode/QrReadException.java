package com.hitachijoho.nextadworld.common.qrcode;

import java.util.HashMap;
import java.util.Map;

/**
 * QRコード読み込み時エラー
 * 
 * @author 100973
 *
 */
public class QrReadException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9109253394683025081L;

	private String errCode;
	private String detail;

	private final static Map<String, String> ERROR_MESSAGES = new HashMap<String, String>();

	static {
		ERROR_MESSAGES.put("01", "ヘッダエラー。別なQRコードが読み込まれました。");
		ERROR_MESSAGES.put("02", "");
		ERROR_MESSAGES.put("", "");
		ERROR_MESSAGES.put("", "");
		ERROR_MESSAGES.put("", "");
	};

	public String getErrorCode() {
		return errCode;
	}

	public String getErrorDetail() {
		return detail;
	}

	public QrReadException(String errCode, String errCause) {
		this.errCode = errCode;
		this.detail = errCause;
	}

	public QrReadException(Throwable e) {
		this.errCode = e.getClass().getName();
		this.detail = e.getMessage();
	}
}
