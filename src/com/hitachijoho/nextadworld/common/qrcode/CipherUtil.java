package com.hitachijoho.nextadworld.common.qrcode;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
/**
 * 暗号化ユーティリティ
 * 
 * @author 100973
 *
 */
public class CipherUtil {

	/**
	 * 自治体固有の共通鍵。実際はプロパティファイルから読み出す等に変更要。
	 */
	private static String CRYPT_KEY = "encodekey1234567";
	private static String CRYPT_IV="0000000012345678";

	/**
	 * 暗号化する。
	 * 
	 * @param str
	 * @return
	 * @throws QrReadException
	 */
	public static String encrypt(String str) throws QrReadException {
		try {
			byte[] bytes = str.getBytes();
	        byte[] iv_raw = CRYPT_IV.getBytes();
			SecretKeySpec secretkey = new SecretKeySpec(CRYPT_KEY.getBytes(Charset.forName("UTF-8")), "AES");
	        IvParameterSpec iv = new IvParameterSpec(iv_raw);
			Cipher cph = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cph.init(Cipher.ENCRYPT_MODE, secretkey,iv);
			byte[] tmp = cph.doFinal(bytes);
			String hex = DatatypeConverter.printHexBinary(tmp);
			return hex;
		} catch (InvalidKeyException e) {

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (InvalidAlgorithmParameterException e) {
			throw new QrReadException(e);
		}
		return "";
	}

	/**
	 * 復号する。
	 * 
	 * @param str
	 * @return
	 * @throws QrReadException
	 */
	public static String decrypt(String str) throws QrReadException {
		try {
	        byte[] iv_raw = CRYPT_IV.getBytes();
			SecretKeySpec secretkey = new SecretKeySpec(CRYPT_KEY.getBytes(Charset.forName("UTF-8")), "AES");
	        IvParameterSpec iv = new IvParameterSpec(iv_raw);
			Cipher cph = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] bytes = DatatypeConverter.parseHexBinary(str);
			cph.init(Cipher.DECRYPT_MODE, secretkey,iv);
			byte[] tmp = cph.doFinal(bytes);
			return new String(tmp,"UTF-8");
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new QrReadException(e);
		}
	}
}
