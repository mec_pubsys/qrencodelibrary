package com.hitachijoho.nextadworld.common.qrcode;

/**
 * 
 * QRコードの共通ヘッダ部分
 * 
 * @author 100973
 *
 */
public class QrHeaderBean {
	private String jcd;
	private String qrtype;
	private String lgapid;
	private Integer userrank;
	private String gendate;
	private String ver;
	private Integer snum;
	public Integer tnum;

	public String getJcd() {
		return jcd;
	}

	public void setJcd(String jcd) {
		this.jcd = jcd;
	}

	public String getQrtype() {
		return qrtype;
	}

	public void setQrtype(String qrtype) {
		this.qrtype = qrtype;
	}

	public String getLgapid() {
		return lgapid;
	}

	public void setLgapid(String lgapid) {
		this.lgapid = lgapid;
	}

	public Integer getUserrank() {
		return userrank;
	}

	public void setUserrank(Integer userrank) {
		this.userrank = userrank;
	}

	public String getGendate() {
		return gendate;
	}

	public void setGendate(String gendate) {
		this.gendate = gendate;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public Integer getSnum() {
		return snum;
	}

	public void setSnum(Integer snum) {
		this.snum = snum;
	}

	public Integer getTnum() {
		return tnum;
	}

	public void setTnum(Integer tnum) {
		this.tnum = tnum;
	}
}
