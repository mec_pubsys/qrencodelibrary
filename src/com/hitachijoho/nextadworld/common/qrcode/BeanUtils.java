package com.hitachijoho.nextadworld.common.qrcode;

import java.lang.reflect.Method;
import java.util.Map;

public class BeanUtils {

	/**
	 * MapからBeanインスタンスに値を編集する
	 * 
	 * @param obj 編集先のオブジェクト
	 * @param map 編集元のMap
	 * @throws QrReadException
	 */
	public static void map2bean(Object obj, Map<String, Object> map) throws QrReadException {
		try {
			Class<? extends Object> clazz = obj.getClass();
			for (Method method : clazz.getMethods()) {
				// setterかどうか、判定
				if (method.getName().startsWith("set") && method.getReturnType() == Void.TYPE
						&& method.getParameterCount() == 1) {

					String name = method.getName().replaceFirst("^set", "").toLowerCase();

					if (map.containsKey(name.toLowerCase()) && isSimpleColumnType(method.getParameterTypes()[0])) {
						// Mapのキーが含まれており、単純な形式のデータならプロパティに格納
						method.invoke(obj, map.get(name));
					} else if (map.containsKey(name.toLowerCase())
							&& map.get(name.toLowerCase()) instanceof Map<?, ?>) {
						// read inner class with Map args and parameter class
						Map<String, Object> child = (Map<String, Object>) map.get(name.toLowerCase());
						Class<?> childClass = method.getParameterTypes()[0];

						// generate child class and append to parent instance.
						Object obj2 = childClass.getDeclaredConstructor(clazz).newInstance(obj);
						map2bean(obj2, child);
						method.invoke(obj, obj2);
					}
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new QrReadException(e);
		}
	}

	/**
	 * プリミティブ型など、プロパティとして普通にあるクラスならtrueを返す。
	 * 
	 * @param clazz
	 * @return プリミティブ型やStringなど、JSONでプロパティとしてありうる場合、true、ありえない＝子クラスの場合、false
	 */
	private static boolean isSimpleColumnType(Class<?> clazz) {
		if (clazz == String.class) {
			return true;
		} else if (clazz == Integer.class) {
			return true;
		} else if (clazz == Long.class) {
			return true;
		} else if (clazz == Double.class) {
			return true;
		} else if (clazz == Float.class) {
			return true;
		} else {
			return false;
		}
	}
}
