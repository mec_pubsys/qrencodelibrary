package com.hitachijoho.nextadworld.common.qrcode;

import java.util.HashMap;
import java.util.Map;

import groovy.json.JsonSlurper;

public class QrCoderEncoder<T> {

	/**
	 * 暗号化したヘッダ部文字列
	 */
	private String cryptedHeader = null;
	
	/**
	 * 読み込まれたデータ部、暗号化済み
	 */
	private Map<Integer, String> importedDatas = new HashMap<Integer, String>();

	/**
	 * QRコードを読み取る。
	 * 
	 * @param json文字列
	 * @return true:読込完了した場合 false:まだ読み込むべきデータが存在する場合。
	 */
	public boolean readQrCode(String json) throws QrReadException {

		JsonSlurper jsonSlurper = new JsonSlurper();
		// JSONテキストを解析
		Map<String, Object> sourceMap = (Map<String, Object>) jsonSlurper.parseText(json);

		// トレーラ部の読込
		int tnum = (Integer) sourceMap.get("tnum");
		int snum = (Integer) sourceMap.get("snum");

		if (cryptedHeader == null) {
			// ヘッダがまだ未読込なら読み込む。
			this.cryptedHeader = (String) sourceMap.get("keys");
			// 初回だけデータ部を全件初期化する.
			this.importedDatas = new HashMap<Integer, String>();
			for (int i = 1; i <= Integer.valueOf(tnum); i++) {
				importedDatas.put(i, null);
			}
		} else {
			// ヘッダが既読込ヘッダと異なっていたらエラーとする
			if (!sourceMap.get("keys").equals(cryptedHeader)) {
				throw new QrReadException("01", sourceMap.get("keys") + "/" + cryptedHeader);
			}
		}

		// Initialize header
		importedDatas.put(snum, (String) sourceMap.get("qrbody"));

		boolean result = true;
		for (int i = 1; i <= Integer.valueOf(tnum); i++) {
			if (importedDatas.get(i) == null) {
				result = false;
			}
		}
		return result;
	}

	/**
	 * 読込対象データ全体の件数を取得する。
	 * 
	 * @return
	 */
	public int getTotal() {
		return importedDatas.size();
	}

	/**
	 * 読込対象データのうち、取り込んだデータ件数を取得する。
	 * 
	 * @return
	 */
	public int getImported() {
		int count = 0;
		for (Map.Entry<Integer, String> entry : importedDatas.entrySet()) {
			if (entry.getValue() != null) {
				count++;
			}
		}
		return count;
	}

	/**
	 * 読み込んだデータを指定オブジェクトのインスタンスで取得する.
	 * 
	 * @return
	 */
	public String getJsonBody() throws QrReadException {
		StringBuilder buff = new StringBuilder();
		for (int i = 1; i <= importedDatas.size(); i++) {
			System.out.println(i + ":" + importedDatas.get(i));
			buff.append(importedDatas.get(i));
		}
		System.out.println(buff.toString());
		String dec = CipherUtil.decrypt(buff.toString());
		return dec;
	}

	public QrHeaderBean getHeader() throws QrReadException {
		QrHeaderBean header = new QrHeaderBean();
		JsonSlurper jsonSlurper = new JsonSlurper();
		String dec = CipherUtil.decrypt(cryptedHeader);
		Map<String, Object> map = (Map<String, Object>) jsonSlurper.parseText(dec);
		BeanUtils.map2bean(header, map);
		return header;
	}

	/**
	 * 読み込んだデータを指定オブジェクトのインスタンスで取得する.
	 * 
	 * @return
	 */
	public Map<String, Object> getDataMap() throws QrReadException {
		String jsonBody = getJsonBody();
		JsonSlurper jsonSlurper = new JsonSlurper();
		Map<String, Object> result = (Map<String, Object>) jsonSlurper.parseText(jsonBody);
		return result;
	}

	/**
	 * 読み込んだデータを指定オブジェクトのインスタンスで取得する.
	 * 
	 * @return
	 */
	public T getData(T obj) throws Throwable {
		BeanUtils.map2bean((Object) obj, getDataMap());
		return obj;
	}
}
